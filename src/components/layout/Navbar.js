import React from 'react';
import {Link} from 'react-router-dom';
import SignedInLinks from './signedInLinks';
import SignedOutLinks from './signedOutLinks';

const Nevbar =() => {
    return(
      <nav className="nav-wrapper grey darker +3">
        <div className="container">
            <Link to="./" className="brand-logo">Project Title</Link>
            <SignedInLinks />
            <SignedOutLinks />
        </div>

      </nav>
    )
}
export default Nevbar;