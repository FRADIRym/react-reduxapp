import React, { Component } from 'react'

 class CreateProject extends Component {
     state={
         title:'', 
         Content:''

     }
     handelChange = (e)=> {
         this.setState( { [e.target.id]:e.target.value});
         
        
     }
     handelSubmit=(e)=>{
         e.preventDefault();   
         console.log(this.state);   
     }
    render() {
        return (
            <div className="container"> 
            <form className="white" onSubmit={this.handelSubmit} >
                <h5 className="grey-text text-darken-3"> create new project </h5>

                <div className="input-fieled" > 
                <label htmlFor="title">title</label>
                <input type="text" id="title"  onChange={this.handelChange}/>
                </div>

                <div className="input-fieled"> 
                <label htmlFor="Content">Content</label>
                <textarea className="materialize-textarea" id="Content" onChange={this.handelChange}> </textarea>
                </div>
                <di className="input-field">
                  <button className="btn pink lighten-1 z-depth-0"> add project </button>
                </di>

            </form>
                
            </div>
        )
    }
}

export default CreateProject;
