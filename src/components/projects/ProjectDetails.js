import React from 'react'

function ProjectDetails (props){
    const id = props.match.params.id;

    return (
        
        <div className="container section project-details">
            <div className="card z-deth-o">
                <div className="card-content">
                  <span className="card-title"> project title - {id} </span>
                  <p> content </p>
                 </div> 
                 <div className="card-action grey lighten-4 grey-text">
                     <div>posted by FR </div>
                     <div>date </div>
                     </div>  
                 
             </div>
            
        </div>
    );
}

export default ProjectDetails;

