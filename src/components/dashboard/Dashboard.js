import React , {Component}  from 'react';
// import Notifications from './Notifications'
import ProjectListe from '../projects/ProjectListe'
import { connect } from 'react-redux'


class Dashboard extends Component {
   
    render() { 
        
        return ( 
            <div className="dashboard container">
                <div className="row">
                    <div className="col s12 m6"> <ProjectListe projects  ={this.props.projects} /> </div>
                   

                </div>

            </div>
         );
    }
    
    
}
//  <div className="col s12 m5 offset -m1"> <Notifications /> </div> 
const mapStateToProps=(State)=>{
return ( { projects: State.project.projects})
}

 
export default connect(mapStateToProps) (Dashboard);
