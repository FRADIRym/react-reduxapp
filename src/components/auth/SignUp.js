import React, { Component } from 'react'

 class SignUp extends Component {
     state={
         email:'', 
         password:'', 
         firstName:'',
         lastName:''

     }
     handelChange = (e)=> {
         this.setState( { [e.target.id]:e.target.value});
         
        
     }
     handelSubmit=(e)=>{ console.log(this.state);   
     }
    render() {
        return (
            <div className="container"> 
             <form className="white" onSubmit={this.handelSubmit} >
                <h5 className="grey-text text-darken-3"> Sign Up </h5>

                <div className="input-fieled" > 
                <label htmlFor="email">E-mail</label>
                <input type="email" id="email"  onChange={this.handelChange}/>
                </div>

                <div className="input-fieled"> 
                <label htmlFor="password">password</label>
                <input type="password" id="password" onChange={this.handelChange} />
                </div>

                <div className="input-fieled" > 
                <label htmlFor="text">firstName</label>
                <input type="text" id="firstName"  onChange={this.handelChange}/>
                </div>

                <div className="input-fieled" > 
                <label htmlFor="text">lastName</label>
                <input type="text" id="lastName"  onChange={this.handelChange}/>
                </div>

                <div className="input-field">
                  <button className="btn pink lighten-1 z-depth-0"> Sign up</button>
                </div>

             </form>
                
            </div>
        )
    }
}

export default SignUp;
