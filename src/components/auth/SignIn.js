import React, { Component } from 'react'

 class SignIn extends Component {
     state={
         email:'', 
         password:''

     }
     handelChange = (e)=> {
         this.setState( { [e.target.id]:e.target.value});
         
        
     }
     handelSubmit=(e)=>{
         e.preventDefault();   
        
     }
    render() {
        return (
            <div className="container"> 
            <form className="white" onSubmit={this.handelSubmit} >
                <h5 className="grey-text text-darken-3"> Sign In </h5>

                <div className="input-fieled" > 
                <label htmlFor="email">E-mail</label>
                <input type="email" id="email"  onChange={this.handelChange}/>
                </div>

                <div className="input-fieled"> 
                <label htmlFor="password">password</label>
                <input type="password" id="password" onChange={this.handelChange} />
                </div>
                <di className="input-field">
                  <button className="btn pink lighten-1 z-depth-0"> Log In</button>
                </di>

            </form>
                
            </div>
        )
    }
}

export default SignIn
